// (function(cordova){
//    var EMS = function() {};
//
//    EMS.prototype.isGPSEnabled = function(success, fail) {
//      return cordova.exec(function(args) {
//          success(args);
//      }, function(args) {
//          fail(args);
//      }, 'EMS', 'isGPSEnabled', []);
//    };
//
//    EMS.prototype.goToGPSSettings = function(success, fail) {
//      return cordova.exec(function(args) {
//          success(args);
//      }, function(args) {
//          fail(args);
//      }, 'EMS', 'goToGPSSettings', []);
//    };
//
//    EMS.prototype.checkGPS = function(success, fail) {
//      return cordova.exec(function(args) {
//          success(args);
//      }, function(args) {
//          fail(args);
//      }, 'EMS', 'checkGPS', []);
//    };
//
//    window.EMS = new EMS();
//
//    // backwards compatibility
//    window.plugins = window.plugins || {};
//    window.plugins.EMS = window.EMS;
//})(window.PhoneGap || window.Cordova || window.cordova);


var exec = require('cordova/exec');

var LOG_TAG = "VkFeeds.js PLUGIN: ";

var VkFeeds = {
	testVar: 'its test ems var',
	init: function (successCallback) {
		console.log(LOG_TAG + 'call init()');
		var cb = function (data) {
			console.log(LOG_TAG + 'succ: ' + data);
			if (successCallback !== null || typeof successCallback !== 'undefined') {
				var d = data;
				if (typeof d.eventData !== 'undefined') {
					d.eventData = JSON.parse(d.eventData);
				}
				successCallback(d);
			}
		};
		exec(cb, cb, "VkFeeds", "init", []);
	},

	sendPacket: function (eventName, data, successCallback, errorCallback) {
		console.log(LOG_TAG + 'call sendPacket(' + eventName + ',' + JSON.stringify(data) + ')');
		exec(function () {
			successCallback || successCallback();
		}, function () {
			errorCallback || errorCallback();
		}, "VkFeeds", "sendPacket", [eventName, data]);
	}
};
console.log(LOG_TAG + 'initing...');

module.exports = VkFeeds;