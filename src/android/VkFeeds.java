package com.trippyhippie.plugin.vkfeeds;

import org.apache.cordova.CordovaActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.*;
import android.provider.Settings;
import android.app.*;
import android.location.*;
import android.net.Uri;
import android.text.Html;
import android.widget.Toast;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaResourceApi;
import org.apache.cordova.PluginResult;

public class VkFeeds extends CordovaPlugin {

    private String MY_SEND_PACKET_ACTION = "vk_feeds_SENDPACKETACTION";
    private String MY_RECEIVE_PACKET_ACTION = "vk_feeds_RECEIVEPACKETACTION";
    private MyReceivedPacketReceiver myReceivedPacketReceiver;

    public VkFeeds() {
    }

    private CallbackContext onNewIntentCallbackContext = null;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {

        if (action.equals("init")) {
            if (onNewIntentCallbackContext == null) {
                myReceivedPacketReceiver = new MyReceivedPacketReceiver(this);
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(MY_RECEIVE_PACKET_ACTION);
                cordova.getActivity().registerReceiver(myReceivedPacketReceiver, intentFilter);

                onNewIntentCallbackContext = callbackContext;

                PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
                result.setKeepCallback(true);
                callbackContext.sendPluginResult(result);


                System.out.println("VkFeeds CordovaPlugin call init");
                cordova.getActivity().startService(new Intent(cordova.getActivity(), MainService.class));
//            actionIsEnableGPS(args, callbackContext);
            }
            return true;
        } else if (action.equals("sendPacket")) {
            try {
                Intent intent = new Intent();
                intent.setAction(MY_SEND_PACKET_ACTION);
                String eventName = args.getString(0);
                intent.putExtra("EVENT_NAME", eventName);
                JSONObject data = args.getJSONObject(1);
                if (data != null) {
                    intent.putExtra("EVENT_DATA", data.toString());
                    System.out.println("VkFeeds CordovaPlugin send: " + eventName + " - " + data);
                }
                else {
                    System.out.println("VkFeeds CordovaPlugin send: " + eventName + " - NO DATA");
                }
                cordova.getActivity().sendBroadcast(intent);
            }
            catch (JSONException exception){
                System.out.println("VkFeeds CordovaPlugin send exception: " + exception.toString());
            }
//            actionGoToGPSSettings(args, callbackContext);
            return true;
        } else if (action.equals("checkGPS")) {
//            actionCheckGPS(args, callbackContext);
            return true;
        }

        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.INVALID_ACTION));
        return false;
    }

    public void sendCallback(String packet) {
        if (onNewIntentCallbackContext != null) {
            try {
                JSONObject object = new JSONObject(packet);
                PluginResult result = new PluginResult(PluginResult.Status.OK, object);
                result.setKeepCallback(true);
                onNewIntentCallbackContext.sendPluginResult(result);
            }
            catch (JSONException exception){
                System.out.println("VkFeeds CordovaPlugin sendCB exception: " + exception.toString());
            }
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
//        if (this.onNewIntentCallbackContext != null) {
//            PluginResult result = new PluginResult(PluginResult.Status.OK, intent.getDataString());
//            result.setKeepCallback(true);
//            this.onNewIntentCallbackContext.sendPluginResult(result);
//        }
    }


    private class MyReceivedPacketReceiver extends BroadcastReceiver {

        VkFeeds vkFeeds;

        public MyReceivedPacketReceiver(com.trippyhippie.plugin.vkfeeds.VkFeeds vkFeeds) {
            this.vkFeeds = vkFeeds;
            System.out.println("MyReceivedPacketReceiver: constructor");
        }

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            String packet = arg1.getStringExtra("PACKET");
            System.out.println("MyReceivedPacketReceiver: receive: " + packet);

            if (vkFeeds != null) {
                vkFeeds.sendCallback(packet);
            }

            /*Toast.makeText(vkFeeds.cordova.getActivity(),
                    "MyReceivedPacketReceiver!\n"
                            + "Event: " + String.valueOf(packet),
                    Toast.LENGTH_LONG).show();
			*/
        }

    }

}
