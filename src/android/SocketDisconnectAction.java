package com.etrans.cordova.plugin.emsbase;

import android.util.Log;

import io.socket.emitter.Emitter;

public class SocketDisconnectAction implements Emitter.Listener {

    private static final String LOG_TAG = "SOCKET disconnect";

    @Override
    public void call(Object... args) {
//        RemoteController.getInstance().restart();
        MainService.sendCallback("socket disconnect");
        Log.d(LOG_TAG, "call");
    }
}
