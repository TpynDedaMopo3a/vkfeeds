package com.etrans.cordova.plugin.emsbase;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.socket.emitter.Emitter;

public class SocketGetJobsSuccessAction implements Emitter.Listener {

    private static final String LOG_TAG = "SOCKET GetJobsSucc";

    @Override
    public void call(Object... args) {
        //event: login:success;
        // data: {
        // "DriverLogin":"anatoliy_driver",
        // "EmtLogin":"anatoliy_emt",
        // "TerminalName":"cordova_test",
        // "CompanyName":"Assist Ambulance",
        // "DriverName":"driver, anatoliy",
        // "EmtName":"emt, anatoliy",
        // "Imei":"cordova_test",
        // "DomainObjectId":"59517779-fe30-4522-9592-6af6e194a33d"
        // }
//        RemoteController.getInstance().restart();
//        try{
        JSONArray jsonArray = (JSONArray) args[0];
        System.out.println("get jobs:success: " + jsonArray.toString());
        MainService.sendCallback("get jobs:success", jsonArray.toString());
//        }
//        catch (JSONException exception){
//            System.out.println("login:success exception: " + exception.toString());
//        }
        Log.d(LOG_TAG, "call: " + args.length);
    }
}
