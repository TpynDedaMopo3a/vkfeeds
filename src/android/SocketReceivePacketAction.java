package com.trippyhippie.plugin.vkfeeds;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.socket.emitter.Emitter;

public class SocketReceivePacketAction implements Emitter.Listener {

    private static final String LOG_TAG = "SOCKET receivePacket";

    @Override
    public void call(Object... args) {
//        try{
        JSONObject object = (JSONObject) args[0];
        MainService.sendCallback(object.toString());
//        }
//        catch (JSONException exception){
//            System.out.println("login:success exception: " + exception.toString());
//        }
        Log.d(LOG_TAG, "call: " + object.toString());
    }
}
