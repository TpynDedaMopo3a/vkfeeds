package com.etrans.cordova.plugin.emsbase;

import android.util.Log;

import io.socket.emitter.Emitter;

public class SocketLoginFailedAction implements Emitter.Listener {

    private static final String LOG_TAG = "SOCKET loginFail";

    @Override
    public void call(Object... args) {
//        RemoteController.getInstance().restart();
        MainService.sendCallback("login:failed");
        Log.d(LOG_TAG, "call");
    }
}
